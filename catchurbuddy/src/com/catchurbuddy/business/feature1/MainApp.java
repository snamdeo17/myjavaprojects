package com.catchurbuddy.business.feature1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {
	public static void main(String[] args) {
		ApplicationContext context= new ClassPathXmlApplicationContext("Beans.xml");
		HelloWorld hw = context.getBean("helloWorld", HelloWorld.class);
		String message = hw.getMessage();
		System.out.println(message);
	}
	
}
